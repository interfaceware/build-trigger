function main(data)
   local commit = component.fields()["Commit Hash"]:trimWS()
   if commit == '' then
      return
   end
   -- Push commit to Build Runner's build queue.
   local task={
      hash=commit, 
      author="Build Server", 
      comment="Purposely triggered build"
   }
	queue.push{data=json.serialize{data=task, compact=true}}
   local event = "Triggered Iguana X build on "..commit
   iguana.logInfo(event)
   component.setStatus{data=event}
end
